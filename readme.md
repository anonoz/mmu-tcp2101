# TCP2101 Algorithm Assignment

## Question

Perform a comparative analysis for finding all-pairs shortest paths between two algorithms: **Dijkstra’s algorithm** and **Floyd-Warshall algorithm**. The comparative analysis should cover the following aspects:

1. Correct implementation. Output the result of finding all-pairs shortest paths between two algorithms for a graph with only 5 vertices. This is for the lecturer/tutor to inspect the correctness of algorithms. [3m]
2. Different numbers of vertices (10, 20, 100, 200, etc) [2m]
3. Different numbers of edges (max. n(n-1)/2) [3m]
4. Randomly assigned weight to each edge (random values between 2 - 20) [2m]
5. In your report, show your findings by plotting the results in graphs (running time
vs graph size). [3m]
6. Conclude your findings in the report. [3m] 7. Presentation. [4m]

## Instruction

Use CMake. I am not going to teach you how to use it here.

```
$ make
$ ./program
```
