#ifndef ADJACENCY_MATRIX_HPP
#define ADJACENCY_MATRIX_HPP

class AdjacencyMatrix {
  private:
    int size;
    int **matrix;

  public:
    AdjacencyMatrix(int size);
    AdjacencyMatrix(int size, double density);
    AdjacencyMatrix(int array[][5]);
    int getSize();
    int * getVertex(int vertex_id);
    void printMatrix();
};

#endif
