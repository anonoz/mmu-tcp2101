#include <iostream>
#include <chrono>
#include "chrono_io"
#include "FloydWarshall.hpp"

FloydWarshall::FloydWarshall(AdjacencyMatrix * arg_graph) {
  // Timer
  typedef std::chrono::high_resolution_clock Clock;
  auto t1 = Clock::now();

  graph = arg_graph;

  std::cout << "Floyd Warshall Algorithm";

  // init distances
  int graph_size = graph->getSize();
  distances = new int* [graph_size];
  
  for (int i = 0; i < graph_size; i++) {
    distances[i] = new int[graph_size];
    for (int j = 0; j < graph_size; j++) {
      if (i == j) {
        distances[i][j] = 0;
      } else if (graph->getVertex(i)[j] > 0) {
        distances[i][j] = graph->getVertex(i)[j];
      } else {
        distances[i][j] = INT_MAX / 2;
      }
    }
  }

  // k, i, j loop, refer to pseudocode on
  // https://en.wikipedia.org/wiki/Floyd–Warshall_algorithm
  for (int k = 0; k < graph_size; k++) {
    for (int i = 0; i < graph_size; i++) {
      for (int j = 0; j < graph_size; j++) {
        if (distances[i][j] > distances[i][k] + distances[k][j]) {
          distances[i][j] = distances[i][k] + distances[k][j];
        }
      }
    }
  }

  std::cout << " Done!\n";
  auto t2 = Clock::now();
  // setTimeTaken(t2 - t1);
  std::cout << "Took " << std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1) << "\n";
}

int FloydWarshall::getDistance(int from_vertex, int to_vertex) {
  return distances[from_vertex][to_vertex];
}

void FloydWarshall::printDistances() {
  for (int from_id = 0; from_id < graph->getSize(); from_id++) {
    std::cout << "From " << from_id << ": {";

    for (int to_id = 0; to_id < graph->getSize(); to_id++) {
      std::cout << to_id << "=> " << distances[from_id][to_id] << "\t";
    }

    std::cout << "}\n";
  }
}

void FloydWarshall::setTimeTaken(std::ostream out) {
  std::stringstream ss;
  ss << out.rdbuf();
  this->time_taken = ss.str();
}

std::string FloydWarshall::getTimeTaken() {
  return time_taken;
}
