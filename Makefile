CFLAGS = -g -Wall -std=c++11
TARGET = program

all: $(TARGET)

$(TARGET): DijkstraAlgorithm.o FloydWarshall.o AdjacencyMatrix.o
	g++ $(CFLAGS) DijkstraAlgorithm.o FloydWarshall.o AdjacencyMatrix.o main.cpp -o $(TARGET)

DijkstraAlgorithm.o: AdjacencyMatrix.o
	g++ $(CFLAGS) -c DijkstraAlgorithm.cpp

FloydWarshall.o: AdjacencyMatrix.o
	g++ $(CFLAGS) -c FloydWarshall.cpp

AdjacencyMatrix.o:
	g++ $(CFLAGS) -c AdjacencyMatrix.cpp

clean:
	rm *.o program 
	rm -rf program.dSYM
