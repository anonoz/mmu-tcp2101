#include <iostream>
#include <sstream>

#include "AdjacencyMatrix.hpp"
#include "DijkstraAlgorithm.hpp"
#include "FloydWarshall.hpp"

using namespace std;

int cin_int(void)
{
  std::string input = "";
  int myNumber = 0;

  while (true) {
    getline(std::cin, input);
    std::stringstream myStream(input);
    if (myStream >> myNumber)
    break;
  }
  return myNumber;    
}

double cin_double(void)
{
  std::string input = "";
  double myNumber = 0;

  while (true) {
    getline(std::cin, input);
    std::stringstream myStream(input);
    if (myStream >> myNumber && myNumber > 0 && myNumber <= 1) {
      break;
    } else {
      std::cout << "Density must be above 0.00 and below or equal to 1: ";
    }
  }
  return myNumber;    
}

int main() {
  // Try Question 1

  // Digraph
  int sample_matrix[][5] = {
    {0, 9, 0, 0, 0},
    {0, 0, 12, 2, 0},
    {7, 0, 0, 0, 6},
    {0, 0, 0, 0, 3},
    {0, 10, 0, 0, 0}
  };

  AdjacencyMatrix * sample_graph = new AdjacencyMatrix(sample_matrix);
  
  DijkstraAlgorithm * sample_dijkstra = new DijkstraAlgorithm(sample_graph);
  sample_dijkstra->printDistances();
  
  FloydWarshall * sample_floyd = new FloydWarshall(sample_graph);
  sample_floyd->printDistances();

  // Question 2
  std::cout << "Vertices count: ";
  int matrix_size = cin_int();

  std::cout << "Graph density (0.01 - 1.00 * n(n-1)/2): ";
  double graph_density = cin_double();

  AdjacencyMatrix * user_graph = new AdjacencyMatrix(matrix_size, graph_density);
  user_graph->printMatrix();

  DijkstraAlgorithm * user_dijkstra = new DijkstraAlgorithm(user_graph);
  // user_dijkstra->printDistances();
  
  FloydWarshall * user_floyd = new FloydWarshall(user_graph);
  // user_floyd->printDistances();

  return 0;
}
