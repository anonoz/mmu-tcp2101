#include <iostream>
#include "AdjacencyMatrix.hpp"

#ifndef FLOYD_WARSHALL_HPP
#define FLOYD_WARSHALL_HPP

class FloydWarshall {
  private:
    AdjacencyMatrix * graph;
    int ** distances;
    std::string time_taken;

  public:
    FloydWarshall(AdjacencyMatrix*);
    int getDistance(int from_vertex, int to_vertex);
    void printDistances();
    void setTimeTaken(std::ostream);
    std::string getTimeTaken();
};

#endif
