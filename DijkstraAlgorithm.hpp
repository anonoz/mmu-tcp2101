#include <iostream>
#include "AdjacencyMatrix.hpp"

#ifndef DIJKSTRA_ALGORITHM_HPP
#define DIJKSTRA_ALGORITHM_HPP

class DijkstraAlgorithm {
  private:
  	AdjacencyMatrix * graph;
    int ** distances;
    bool ** visited;
    std::string time_taken;
    int calculateDistance(int from_vertex, int to_vertex);
    
  public:
    DijkstraAlgorithm(AdjacencyMatrix*);
    int getDistance(int from_vertex, int to_vertex);
    void printDistances();
    void setTimeTaken(std::ostream);
    std::string getTimeTaken();
};

#endif
