#include <iostream>
#include <ctime>
#include "AdjacencyMatrix.hpp"

#define MIN_EDGE_WEIGHT 2
#define MAX_EDGE_WEIGHT 20

// Empty matrix
AdjacencyMatrix::AdjacencyMatrix(int size) {
  this->size = size;
  matrix = new int* [size];

  for (int i = 0; i < size; i++) {
    matrix[i] = new int [size];
    for (int j = 0; j < size; j++) {
      matrix[i][j] = 0;
    }
  }
}

// Random generated matrix
AdjacencyMatrix::AdjacencyMatrix(int size,
                                 double density) {
  this->size = size;
  matrix = new int* [size];

  srand(time(NULL));

  // 1st loop: Initialise the matrix
  for (int i = 0; i < size; i++) {
    matrix[i] = new int[size];
    for (int j = 0; j < size; j++) {
      matrix[i][j] = 0;
    }
  }

  // 2nd loop: Populate it with edges
  for (int i = 0; i < size; i++) {
    matrix[i] = new int[size];
    double lottery_handicap = (double)(i + 1) / size;

    for (int j = 0; j < size; j++) {
      // Even with density = 1, we need to make sure
      // not all edges come out from first vertices
      // So the larger vertices get the chance to go to
      // other vertices.
      double lottery = (rand() % 100) / 100.0 / lottery_handicap;

      // 2nd condition is to ensure there is only 1 edge
      // between two vertices.
      if (i != j && lottery <= density && matrix[j][i] == 0) {
        // The edge's weight has to be between 2 and 20
        int weight = rand() % (MAX_EDGE_WEIGHT - MIN_EDGE_WEIGHT) + MIN_EDGE_WEIGHT;
        matrix[i][j] = weight;

        lottery_handicap *= density * 0.75;
      } else {
        // increase handicap for i
        lottery_handicap *= 1 + (density * 0.75);
      }
    }
  }
}

AdjacencyMatrix::AdjacencyMatrix(int array[][5]) {
  this->size = 5;
  // matrix = (int **)array;
  matrix = new int* [size];
  // matrix = (int **)array;

  for (int i = 0; i < size; i++) {
    matrix[i] = new int[size];
    for (int j = 0; j < size; j++) {
      matrix[i][j] = array[i][j];
    }
  }

  std::cout << "Initialized sample matrix \n";
}

int AdjacencyMatrix::getSize() {
  return size;
}

int * AdjacencyMatrix::getVertex(int vertex_id) {
  return matrix[vertex_id];
}

void AdjacencyMatrix::printMatrix() {
  // Header row
  std::cout << "\t";
  for (int i = 0; i < getSize(); i++) {
    std::cout << i << "\t";
  }
  std::cout << "\n";

  // Body rows
  for (int i = 0; i < getSize(); i++) {
    std::cout << i << "\t";
    for (int j = 0; j < getSize(); j++) {
      std::cout << matrix[i][j] << "\t";
    }
    std::cout << "\n";
  }
}
