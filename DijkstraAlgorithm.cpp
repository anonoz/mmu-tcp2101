#include <iostream>
#include <list>
#include <chrono>
#include "chrono_io"
#include "DijkstraAlgorithm.hpp"

#define MY_INFINITY -9999

DijkstraAlgorithm::DijkstraAlgorithm(AdjacencyMatrix * arg_graph) {
  // Timer
  typedef std::chrono::high_resolution_clock Clock;
  auto t1 = Clock::now();

  graph = arg_graph;

  // Computations are done when you load a graph
  std::cout << "Dijksra Algorithm";

  // Initialise vertices as unvisited + infinity and beyond
  // std::cout << MY_INFINITY;
  int graph_size = graph->getSize();
  distances = new int* [graph_size];
  visited = new bool* [graph_size];

  for (int i = 0; i < graph_size; i++) {
    distances[i] = new int[graph_size];
    visited[i] = new bool[graph_size];

    for (int j = 0; j < graph_size; j++) {

      // If i == j, that means it's a vertex to itself, distance = 0
      distances[i][j] = (i == j) ? 0 : MY_INFINITY;

      // Same vertex considered visited itself
      visited[i][j] = (i == j);
    }
  }

  // Loop over "from" vertices
  for (int from_vertex_id = 0; from_vertex_id < graph_size; from_vertex_id++) {    
    std::list<int> q;
    int * from_vertex = graph->getVertex(from_vertex_id);

    // "Initialise" visiting queue with edges connected to from_vertex
    // As well as setting distance between from_vertex to direct neighbour
    for (int probe_vertex_id = 0; probe_vertex_id < graph->getSize(); probe_vertex_id++) {
      if (from_vertex[probe_vertex_id] > 0){
        q.push_back(probe_vertex_id);
        distances[from_vertex_id][probe_vertex_id] = from_vertex[probe_vertex_id];
      }
    }

    // Let the loop begin
    while (!q.empty()) {
      // Choose the next nearest neighbour to visit
      int chosen_vertex_id = -1;
      int min_distance = MY_INFINITY;
      for (std::list<int>::iterator it = q.begin(); it != q.end(); ++it) {
        if (min_distance == MY_INFINITY || graph->getVertex(from_vertex_id)[*it] < min_distance) {
          chosen_vertex_id = *it;
          min_distance = distances[from_vertex_id][chosen_vertex_id];
        }
      }

      // Remove this from q
      q.remove(chosen_vertex_id);
      int * chosen_vertex = graph->getVertex(chosen_vertex_id);

      int chosen_distance = distances[from_vertex_id][chosen_vertex_id];

      // Find neighbours
      for (int probe_v_id = 0; probe_v_id < graph->getSize(); probe_v_id++) {
        if (chosen_vertex[probe_v_id] > 0 && from_vertex_id != probe_v_id) {
          // get from_vertex to probe_vertex
          int new_probe_distance = chosen_distance + chosen_vertex[probe_v_id];
          int old_probe_distance = distances[from_vertex_id][probe_v_id];

          if (old_probe_distance == MY_INFINITY 
              || new_probe_distance < old_probe_distance) {
            distances[from_vertex_id][probe_v_id] = new_probe_distance;
          }

          // if this probe has never been visited before, put that to q
          if (MY_INFINITY == old_probe_distance) {
            q.push_back(probe_v_id);
          }
        }
      }
    }
  }

  std::cout << " Done!\n";
  auto t2 = Clock::now();

  std::cout << "Took " << std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1) << "\n";
}

int DijkstraAlgorithm::calculateDistance(int from_vertex, int to_vertex) {
  return distances[from_vertex][to_vertex];
}

void DijkstraAlgorithm::printDistances() {
  for (int from_id = 0; from_id < graph->getSize(); from_id++) {
    std::cout << "From " << from_id << ": {";

    for (int to_id = 0; to_id < graph->getSize(); to_id++) {
      std::cout << to_id << "=> " << distances[from_id][to_id] << "\t";
    }

    std::cout << "}\n";
  }
}

void DijkstraAlgorithm::setTimeTaken(std::ostream out) {
  std::stringstream ss;
  ss << out.rdbuf();
  this->time_taken = ss.str();
}

std::string DijkstraAlgorithm::getTimeTaken() {
  return time_taken;
}
